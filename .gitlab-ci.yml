variables:
  TERM: xterm-color
  TIMEFORMAT: "\n%0lR" # for bash time
  DEBIAN_FRONTEND: noninteractive
  YES_QUIET_APT_OPTS: "--assume-yes --quiet=2 -o=Dpkg::Use-Pty=0"
  DI4AT: registry.gitlab.com/docker-images4automated-tests

image: $DI4AT/debian/sudo


bash:
  before_script:
  - (. /etc/os-release ; echo $PRETTY_NAME)
  - set -o errexit -o nounset -o noglob -o pipefail
  - shopt -s failglob
  script:
  - set +o
  - echo $0
  - echo $BASH_VERSION
  - tty || true
  - ([[ $- == *i* ]] && echo 'Interactive' || echo 'Not interactive')
  - shopt -q login_shell && echo 'Login shell' || echo 'Not login shell' # bash builtin
  - time ${SUDO:-} apt-get update --quiet=2
  # time ${CHRONIC:-} ${SUDO:-} apt-get upgrade --with-new-pkgs $YES_QUIET_APT_OPTS
  - time ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-suggests
            --no-install-recommends
            $YES_QUIET_APT_OPTS
        procps 
  - command -v uptime # procps
  - free # procps


function:
    script:
    -   |
        e() {
            echo $@
        }
    - e fg


bpkg:
    script:
    # - curl -sLo- http://get.bpkg.sh | bash
    - curl http://get.bpkg.sh
    - curl https://raw.githubusercontent.com/bpkg/bpkg/master/setup.sh > setup.sh
    - ls
    - bash setup.sh
    - (! env PATH=~/.local/bin:$PATH bash -c "bpkg")
    - env PATH=~/.local/bin:$PATH bash -c "bpkg -h"
    - env PATH=~/.local/bin:$PATH bash -c "bpkg -V"
    - env PATH=~/.local/bin:$PATH bash -c "bpkg install -g term"
    - env PATH=~/.local/bin:$PATH bash -c "bpkg -h"
    - (! env PATH=~/.local/bin:$PATH bash -c "term")
    before_script:
    - set -o errexit -o nounset -o noglob -o pipefail
    - shopt -s failglob
    - (. /etc/os-release ; echo $PRETTY_NAME)

    - ${SUDO:-} apt-get update --quiet=2
    - time ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-suggests
            --no-install-recommends
            --quiet=2
        ca-cacert
        curl
        git
    image: ubuntu:rolling


colors:
    # See also command msgcat in package gettext
    script:
    -   |
        #Background
        for clbg in {40..47} {100..107} 49 ; do
        	#Foreground
        	for clfg in {30..37} {90..97} 39 ; do
        		#Formatting
        		for attr in 0 1 2 4 5 7 ; do
        			#Print the result
        			echo -en "\e[${attr};${clbg};${clfg}m ^[${attr};${clbg};${clfg}m \e[0m"
        		done
        		echo #Newline
        	done
        done
    -   |
        for fgbg in 38 48 ; do # Foreground / Background
            for color in {0..255} ; do # Colors
                # Display the color
                printf "\e[${fgbg};5;%sm  %3s  \e[0m" $color $color
                # Display 6 colors per lines
                if [ $((($color + 1) % 6)) == 4 ] ; then
                    echo # New line
                fi
            done
            echo # New line
        done

# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

# https://misc.flogisoft.com/bash/tip_colors_and_formatting
# https://docs.gitlab.com/ee/ci/yaml/script.html 




logout-file: # See also trap-logout.
  script:
  - trap 'xs=$? ; . $HOME/.sh_logout ; exit $xs' 0
  - trap
  - (true)
  # This could be added in .profile in docker file.
  # Or
  # trap '. /etc/sh_logout; exit' 0
  # In /etc/profile


set:
  script:
  - set -o
  # set -eufo pipefail ... longer version:
  - set -o errexit -o nounset -o noglob -o pipefail
  - set -o
  - shopt -s failglob || true


trap-logout-command-line:
  script:
  - bash -c "trap 'echo bye' EXIT ; trap"
  # - bash <<<': | { eval "trap echo\ bye EXIT"; }'


trap-logout-function-true:
  script:
  - |
    bye() { echo bye ; }
  - bye
  - trap bye EXIT
  - trap



trap-logout-false:
  script:
  - trap 'xs=$? ; echo $xs ; echo bye! ; exit $xs' 0
  - trap
  - (false)
  allow_failure: true


trap-logout-true:
  script:
  - trap 'xs=$? ; echo $xs ; echo bye! ; exit $xs' 0
  - trap
  - (true)


trap-logout-short-true:
  script:
  - trap "echo bye!" EXIT
  - trap
  - (true)

trap-logout-short-to-file-true:
  script:
  - echo $$
  - pstree --arguments --show-pids --thread-names --uid-changes --unicode
  - pwd
  - trap "echo bye! > bye.txt" EXIT
  - trap
  - exit
  after_script:
  - pwd
  - ls
  - echo $$
  - pstree --arguments --show-pids --thread-names --uid-changes --unicode
  before_script:
  - ${SUDO:-} apt-get update --quiet=2
  - time ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-recommends
            $YES_QUIET_APT_OPTS
        psmisc

trap-logout-file-short-true:
  script:
  - file ~/.bash_logout
  - echo "echo bye!" >> ~/.bash_logout
  - (true)
  - (! logout)
  - exit
  before_script:
  - ${SUDO:-} apt-get update --quiet=2
  - time ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-recommends
            $YES_QUIET_APT_OPTS
        file

trap-logout-curly-brackets-true:
  script:
  - trap "{ xs=$? ; echo $xs ; echo bye! ; exit $xs ; }" EXIT
  - trap
  - (true)

trap-logout-eval-short-true:
  script:
  - eval "trap echo\ bye EXIT";
  - trap
  - (true)
  - exit

trap-signals:
  script:
  - kill -l
  - eval "trap echo\ ALRM ALRM";
  - eval "trap echo\ ABRT ABRT";
  - eval "trap echo\ BUS BUS";
  - eval "trap echo\ CHLD CHLD";
  - eval "trap echo\ CONT CONT";
  - eval "trap echo\ EXIT EXIT";
  - eval "trap echo\ FPE FPE";
  - eval "trap echo\ HUP HUP";
  - eval "trap echo\ ILL ILL";
  - eval "trap echo\ INT INT";
  - eval "trap echo\ IO IO";
  - eval "trap echo\ KILL KILL";
  - eval "trap echo\ PIPE PIPE";
  - eval "trap echo\ PROF PROF";
  - eval "trap echo\ PWR PWR";
  - eval "trap echo\ QUIT QUIT";
  - eval "trap echo\ SEGV SEGV";
  - eval "trap echo\ STKFLT STKFLT";
  - eval "trap echo\ STOP STOP";
  - eval "trap echo\ SYS SYS";
  - eval "trap echo\ TERM TERM";
  - eval "trap echo\ TRAP TRAP";
  - eval "trap echo\ TSTP TSTP";
  - eval "trap echo\ TTIN TTIN";
  - eval "trap echo\ TTOU TTOU";
  - eval "trap echo\ URG URG";
  - eval "trap echo\ USR1 USR1";
  - eval "trap echo\ USR2 USR2";
  - eval "trap echo\ VTALRM VTALRM";
  - eval "trap echo\ WINCH WINCH";
  - eval "trap echo\ XCPU XCPU";
  - eval "trap echo\ XFSZ XFSZ";
  - trap
  
trap-signals-all:
  script:
  - trap "echo bye ; exit" 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
        20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
        40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59
        60 61 62 63 64
  - trap
